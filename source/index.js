$(document).ready(function(){ // ham load het hmlt ,css 
    MainEvent = new MainEvent();

})


class MainEvent{

    constructor(){ //tao doi tuong cho event
       this.initEvent();


    }

    initEvent(){   // tao event
        //$(".task-item").click(this.toggleTask); // xu ly voi cac task-item co san
        $(document).on( "click",".xoa-item",this.toggleTask); // cac task-item moi cung duoc gan su kien
        
        //thu nho main-left
        $(".top1").click(this.togglethunhomainleft);

        // phuc hoi main-left
        $(".thunho-main-left-mid-groceries").click(this.phuchoimainleft);

        //an main-right khi click vao .icon-close-right
        $(".icon-close-right").click(function(){
            $(".main-right").toggleClass("class-an");
        })
        
        //click vao username --> hien ra  .content-username
        $(".information").click(function(e){
            $("#content-username").toggleClass("class-none");
            e.stopPropagation();
        })

        //click outsite
        $("window").click(function(evt) {
            var target = evt.currentTarget;
            var inside = $("#content-username");
            console.log(target);
            console.log(inside);
            if (target != inside) {
                $("#content-username").removeClass("class-none");
            }
        
        });
    }

                                   // noi dinh  nghia function xu ly su kien

    // doi mua khi click vao cac task-item

    toggleTask(){
        $(this).toggleClass("color-hover");
        $(this).siblings().removeClass("color-hover") // siblings -->chon tat ca cac task-item ko duoc chon
    }

    //thu nho main-left

    togglethunhomainleft(){
        $(".main-left").toggleClass("thunho-main-left");
    }

    //phuc hoi main-left

    phuchoimainleft(){
        $(".main-left").removeClass("thunho-main-left");
    }
}